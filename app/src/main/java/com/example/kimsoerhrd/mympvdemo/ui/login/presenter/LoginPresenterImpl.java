package com.example.kimsoerhrd.mympvdemo.ui.login.presenter;

import com.example.kimsoerhrd.mympvdemo.ui.login.interactor.LoginInteractor;
import com.example.kimsoerhrd.mympvdemo.ui.login.interactor.LoginInteractorImpl;

public class LoginPresenterImpl implements Presenter.LoginPresenter, LoginInteractor.LoginResponeInteractor  {

    public Presenter.LoginView view;
    LoginInteractor interactor;

    public LoginPresenterImpl(Presenter.LoginView view) {
        this.view = view;
        interactor = new LoginInteractorImpl();
    }

    @Override
    public void onDestroyView() {
        if (view!=null){
            view = null;
        }
    }

    @Override
    public void onLoginCredential(String username, String password) {
            if (view!=null){
                interactor.login(username, password, this);
                view.showProgressBar();
            }
    }

    @Override
    public void onUserError() {
        if (view!=null){
            view.onUserError();
            view.hideProgressBar();
        }

    }

    @Override
    public void onPasswordError() {
        if (view!=null){
            view.onPasswordError();
            view.hideProgressBar();
        }

    }

    @Override
    public void onLoginSuccess() {
        if (view!=null){
            view.onLoginSuccess();
            view.hideProgressBar();
        }

    }
}
