package com.example.kimsoerhrd.mympvdemo.ui.login.view;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.kimsoerhrd.mympvdemo.R;
import com.example.kimsoerhrd.mympvdemo.base.BaseActivity;
import com.example.kimsoerhrd.mympvdemo.ui.login.presenter.LoginPresenterImpl;
import com.example.kimsoerhrd.mympvdemo.ui.login.presenter.Presenter;
import com.example.kimsoerhrd.mympvdemo.ui.main.MainActivity;

public class LoginActivity extends BaseActivity implements Presenter.LoginView {
    EditText user, pass;
    Button btnLogin;
    ProgressBar progressBar;

    Presenter.LoginPresenter presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        user = findViewById(R.id.edUser);
        pass = findViewById(R.id.edPassword);
        btnLogin = findViewById(R.id.btnLogin);
        progressBar = findViewById(R.id.progressBar);
        presenter = new LoginPresenterImpl(this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onLoginCredential(user.getText().toString(), pass.getText().toString());
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroyView();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(ProgressBar.INVISIBLE);
    }

    @Override
    public void onUserError() {
        user.setError("Error UserName");

    }

    @Override
    public void onPasswordError() {
        pass.setError("Error Password");
    }

    @Override
    public void onLoginSuccess() {
        Intent intent= new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
