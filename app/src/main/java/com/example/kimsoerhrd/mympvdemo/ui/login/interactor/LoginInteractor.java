package com.example.kimsoerhrd.mympvdemo.ui.login.interactor;

public interface LoginInteractor {

    void login(String username, String password, LoginResponeInteractor responeInteractor);
    interface LoginResponeInteractor{
        void onUserError();
        void onPasswordError();
        void onLoginSuccess();
    }


}
