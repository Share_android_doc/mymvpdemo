package com.example.kimsoerhrd.mympvdemo.ui.login.interactor;

import android.os.Handler;
import android.text.TextUtils;

public class LoginInteractorImpl implements LoginInteractor {
    @Override
    public void login(final String username, final String password, final LoginResponeInteractor responeInteractor) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (TextUtils.isEmpty(username)){
                    responeInteractor.onUserError();
                }else if (TextUtils.isEmpty(password)){
                    responeInteractor.onPasswordError();
                }else{
                    responeInteractor.onLoginSuccess();
                }
            }
        },1000);

    }
}
