package com.example.kimsoerhrd.mympvdemo.ui.login.presenter;

public interface Presenter {

    interface LoginView{
        void showProgressBar();
        void hideProgressBar();
        void onUserError();
        void onPasswordError();
        void onLoginSuccess();

    }

    interface LoginPresenter{
        void onDestroyView();
        void onLoginCredential(String username, String password);
    }
}
